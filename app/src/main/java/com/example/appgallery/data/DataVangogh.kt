package com.example.appgallery.data

import com.example.appgallery.R
import com.example.appgallery.model.Picasso
import com.example.appgallery.model.VanGogh

class DataVangogh {
    fun loadAffirmation1(): List<VanGogh> {
        return listOf<VanGogh>(
            VanGogh(
                R.string.master_painter_4_pic_1,
                R.drawable.vincent_van_gogh_01
            ),
            VanGogh(
                R.string.master_painter_4_pic_2,
                R.drawable.vincent_van_gogh_02
            ),
            VanGogh(
                R.string.master_painter_4_pic_3,
                R.drawable.vincent_van_gogh_03
            ),
            VanGogh(
                R.string.master_painter_4_pic_4,
                R.drawable.vincent_van_gogh_04
            ),
            VanGogh(
                R.string.master_painter_4_pic_5,
                R.drawable.vincent_van_gogh_05
            )
        )
    }
}

