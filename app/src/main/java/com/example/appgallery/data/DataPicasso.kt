package com.example.appgallery.data

import com.example.appgallery.R
import com.example.appgallery.model.Picasso

class DataPicasso {
    fun loadAffirmation1(): List<Picasso> {
        return listOf<Picasso>(
            Picasso(
                R.string.master_painter_2_pic_1,
                R.drawable.pablo_picasso_01
            ),
            Picasso(
                R.string.master_painter_2_pic_2,
                R.drawable.pablo_picasso_02
            ),
            Picasso(
                R.string.master_painter_2_pic_3,
                R.drawable.pablo_picasso_03
            ),
            Picasso(
                R.string.master_painter_2_pic_4,
                R.drawable.pablo_picasso_04
            ),
            Picasso(
                R.string.master_painter_2_pic_5,
                R.drawable.pablo_picasso_05
            )
        )
    }
}

