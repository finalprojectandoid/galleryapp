package com.example.appgallery.data

import com.example.appgallery.R
import com.example.appgallery.model.Leonardo


class DataReonardo {
    fun loadAffirmation1(): List<Leonardo> {
        return listOf<Leonardo>(
            Leonardo(
                R.string.master_painter_1_pic_2,
                R.drawable.leonardo_da_vinci_02
            ),
            Leonardo(
                R.string.master_painter_1_pic_1,
                R.drawable.leonardo_da_vinci_01
            ),
            Leonardo(
                R.string.master_painter_1_pic_3,
                R.drawable.leonardo_da_vinci_03
            ),
            Leonardo(
                R.string.master_painter_1_pic_4,
                R.drawable.leonardo_da_vinci_04
            ),
            Leonardo(
                R.string.master_painter_1_pic_5,
                R.drawable.leonardo_da_vinci_05
            )
        )
    }
}

