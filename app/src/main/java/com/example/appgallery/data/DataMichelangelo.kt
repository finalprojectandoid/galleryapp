package com.example.appgallery.data


import com.example.appgallery.R


class DataMichelangelo{
    fun loadAffirmation1(): List<Michelangelo> {
        return listOf<Michelangelo>(
            Michelangelo(
                R.string.master_painter_3_pic_1,
                R.drawable.michelangelo_01
            ),
            Michelangelo(
                R.string.master_painter_3_pic_2,
                R.drawable.michelangelo_02
            ),
            Michelangelo(
                R.string.master_painter_3_pic_3,
                R.drawable.michelangelo_03
            ),
            Michelangelo(
                R.string.master_painter_3_pic_4,
                R.drawable.michelangelo_04
            ),
            Michelangelo(
                R.string.master_painter_3_pic_5,
                R.drawable.michelangelo_05
            )
        )
    }
}

