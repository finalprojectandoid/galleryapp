package com.example.appgallery.data

import com.example.appgallery.R
import com.example.appgallery.model.HomeGallery

class Datasource {
    fun loadAffirmation() : List<HomeGallery> {
        return listOf<HomeGallery>(
            HomeGallery(R.string.master_painter_1,R.drawable.leonardoimage),
            HomeGallery(R.string.master_painter_2,R.drawable.pablo_picasso_00),
            HomeGallery(R.string.master_painter_3,R.drawable.michelangelo_00),
            HomeGallery(R.string.master_painter_4,R.drawable.vincent_van_gogh_00),
            HomeGallery(R.string.master_painter_5,R.drawable.rembrandt_00),
        )
    }
}