package com.example.appgallery.data

import com.example.appgallery.R
import com.example.appgallery.model.Rembrandt

class DataRembrandt {
    fun loadAffirmation1(): List<Rembrandt> {
        return listOf<Rembrandt>(
            Rembrandt(
                R.string.master_painter_5_pic_1,
                R.drawable.rembrandt_01
            ),
            Rembrandt(
                R.string.master_painter_5_pic_2,
                R.drawable.rembrandt_02
            ),
            Rembrandt(
                R.string.master_painter_5_pic_3,
                R.drawable.rembrandt_03
            ),
            Rembrandt(
                R.string.master_painter_5_pic_4,
                R.drawable.rembrandt_04
            ),
            Rembrandt(
                R.string.master_painter_5_pic_5,
                R.drawable.rembrandt_05
            )
        )
    }
}

