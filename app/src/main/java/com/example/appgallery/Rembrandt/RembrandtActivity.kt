package com.example.appgallery.Rembrandt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.R
import com.example.appgallery.adapter.RembrandtAdapter
import com.example.appgallery.data.DataRembrandt

class RembrandtActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rembrandt_home)
        val myDataRembrandt = DataRembrandt().loadAffirmation1()
        val recyclerViewRembrandt = findViewById<RecyclerView>(R.id.recycleview_rem)
        recyclerViewRembrandt.adapter = RembrandtAdapter(this,myDataRembrandt)
        recyclerViewRembrandt.setHasFixedSize(true)
    }
}