package com.example.appgallery.Leonardodavinci

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.R
import com.example.appgallery.adapter.ReobardoAdapter
import com.example.appgallery.data.DataReonardo


class LeonardoActivit : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leonargo_home)
        val myDataReonardo = DataReonardo().loadAffirmation1()
        val recyclerViewReonardo = findViewById<RecyclerView>(R.id.recycleview_leonardo)
        recyclerViewReonardo.adapter = ReobardoAdapter(this,myDataReonardo)
        recyclerViewReonardo.setHasFixedSize(true)
    }
}