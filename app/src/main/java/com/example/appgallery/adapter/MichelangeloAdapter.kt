package com.example.appgallery.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.*
import com.example.appgallery.Michelangelo.*
import com.example.appgallery.data.Michelangelo

class MichelangeloAdapter (private val context: Context, val dataset: List<Michelangelo>) : RecyclerView.Adapter<MichelangeloAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.mic_name)
        val imageView: ImageView = view.findViewById(R.id.mic_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLaout = LayoutInflater.from(parent.context).inflate(R.layout.activity_michelangelo,parent,false)
        return ItemViewHolder(adapterLaout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView.text = context.resources.getString(item.stringResurceId)
        holder.imageView.setImageResource(item.imageResoureId)
        holder.imageView.setOnClickListener {
            if (position == 0) {
                val context = holder.imageView.context
                val intent = Intent(context, DavidActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 1) {
                val context = holder.imageView.context
                val intent = Intent(context, PietaActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 2) {
                val context = holder.imageView.context
                val intent = Intent(context, MadonnaActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 3) {
                val context = holder.imageView.context
                val intent = Intent(context, DyingSlaveActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 4) {
                val context = holder.imageView.context
                val intent = Intent(context, CreationActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}