package com.example.appgallery.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.*
import com.example.appgallery.Rembrandt.*
import com.example.appgallery.model.Rembrandt

class RembrandtAdapter (private val context: Context, val dataset: List<Rembrandt>) : RecyclerView.Adapter<RembrandtAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = view.findViewById(R.id.rem_name)
        val imageView: ImageView = view.findViewById(R.id.rem_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLaout = LayoutInflater.from(parent.context).inflate(R.layout.activity_rembrandt,parent,false)
        return ItemViewHolder(adapterLaout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView.text = context.resources.getString(item.stringResurceId)
        holder.imageView.setImageResource(item.imageResoureId)
        holder.imageView.setImageResource(item.imageResoureId)
        holder.imageView.setOnClickListener {
            if (position == 0) {
                val context = holder.imageView.context
                val intent = Intent(context, TheNightWatchActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 1) {
                val context = holder.imageView.context
                val intent = Intent(context, LessonActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 2) {
                val context = holder.imageView.context
                val intent = Intent(context, DanaeActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 3) {
                val context = holder.imageView.context
                val intent = Intent(context, TheJewishBrideActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 4) {
                val context = holder.imageView.context
                val intent = Intent(context, BathshebaActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}