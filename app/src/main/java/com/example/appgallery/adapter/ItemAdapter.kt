package com.example.appgallery.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.*
import com.example.appgallery.Leonardodavinci.LeonardoActivit
import com.example.appgallery.Michelangelo.MichelangeloActivity
import com.example.appgallery.PabloPicasso.PabloPicassoActivity
import com.example.appgallery.Rembrandt.RembrandtActivity
import com.example.appgallery.VincentvanGogh.VanGogActivity
import com.example.appgallery.model.HomeGallery

class ItemAdapter(private val context: Context, val dataset: List<HomeGallery>) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view){
        val textView: TextView = view.findViewById(R.id.item_titlw)
        val imageView: ImageView = view.findViewById(R.id.item_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLaout = LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)
        return ItemViewHolder(adapterLaout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView.text = context.resources.getString(item.stringResurceId)
        holder.imageView.setImageResource(item.imageResoureId)
        holder.imageView.setOnClickListener{
            if (position == 0){
                val context = holder.imageView.context
                val intent = Intent(context, LeonardoActivit::class.java)
                context.startActivity(intent)
            }
            if (position == 1){
                val context = holder.imageView.context
                val intent = Intent(context, PabloPicassoActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 2){
                val context = holder.imageView.context
                val intent = Intent(context, MichelangeloActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 3){
                val context = holder.imageView.context
                val intent = Intent(context, VanGogActivity::class.java)
                context.startActivity(intent)
            }
            if (position == 4){
                val context = holder.imageView.context
                val intent = Intent(context, RembrandtActivity::class.java)
                context.startActivity(intent)
            }
        }
    }
    override fun getItemCount(): Int {
        return dataset.size
    }
}