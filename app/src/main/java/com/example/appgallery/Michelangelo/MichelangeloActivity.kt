package com.example.appgallery.Michelangelo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.R
import com.example.appgallery.adapter.MichelangeloAdapter
import com.example.appgallery.data.DataMichelangelo

class MichelangeloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_michelangelo_home)
        val myDataMichelangelo = DataMichelangelo().loadAffirmation1()
        val recyclerViewMichelangelo = findViewById<RecyclerView>(R.id.recycleview_michel)
        recyclerViewMichelangelo.adapter = MichelangeloAdapter(this,myDataMichelangelo)
        recyclerViewMichelangelo.setHasFixedSize(true)
    }
}