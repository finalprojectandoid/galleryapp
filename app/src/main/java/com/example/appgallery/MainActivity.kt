package com.example.appgallery

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    var btnstart : Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnstart = findViewById<Button>(R.id.btnstart)

        btnstart!!.setOnClickListener{
            var intent = Intent(this,HomeActivity::class.java)
            startActivity(intent)
        }
    }
}