package com.example.appgallery.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Picasso(@StringRes val stringResurceId : Int, @DrawableRes val imageResoureId : Int)
