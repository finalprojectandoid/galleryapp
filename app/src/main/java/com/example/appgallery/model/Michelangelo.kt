package com.example.appgallery.data

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Michelangelo(@StringRes val stringResurceId : Int, @DrawableRes val imageResoureId : Int)

