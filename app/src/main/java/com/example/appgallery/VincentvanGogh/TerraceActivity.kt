package com.example.appgallery.VincentvanGogh

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appgallery.R

class TerraceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terrace)
    }
}