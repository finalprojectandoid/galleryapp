package com.example.appgallery.VincentvanGogh

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.R
import com.example.appgallery.adapter.VangoghAdapter
import com.example.appgallery.data.DataVangogh

class VanGogActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_van_gog_home)
        val myDataVangogh = DataVangogh().loadAffirmation1()
        val recyclerViewVangogh = findViewById<RecyclerView>(R.id.recycleview_van)
        recyclerViewVangogh.adapter = VangoghAdapter(this,myDataVangogh)
        recyclerViewVangogh.setHasFixedSize(true)
    }
}