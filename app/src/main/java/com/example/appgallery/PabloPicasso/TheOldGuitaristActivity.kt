package com.example.appgallery.PabloPicasso

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appgallery.R

class TheOldGuitaristActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_the_old_guitarist)
    }
}