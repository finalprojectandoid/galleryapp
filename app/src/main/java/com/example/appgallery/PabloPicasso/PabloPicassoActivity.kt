package com.example.appgallery.PabloPicasso

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.appgallery.R
import com.example.appgallery.adapter.PicassoAdapter
import com.example.appgallery.data.DataPicasso

class PabloPicassoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picasso_home)
        val myDataReonardo = DataPicasso().loadAffirmation1()
        val recyclerViewReonardo = findViewById<RecyclerView>(R.id.recycleview_picasso)
        recyclerViewReonardo.adapter = PicassoAdapter(this,myDataReonardo)
        recyclerViewReonardo.setHasFixedSize(true)
    }
}